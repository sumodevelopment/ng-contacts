import {Component} from '@angular/core';

@Component({
  selector: 'app-contact-not-selected',
  templateUrl: './contact-not-selected.component.html',
  styles: [
    `.contact-not-selected {
      color: var(--col-grey-dark);
      padding: 2em;
      display: flex;
      flex-direction: column;
      justify-content: center;
      align-items: center;
    }
    .contact-not-selected h4 {
      margin-bottom: 0;
    }
    .material-icons {
      font-size: 2em;
    }
    `
  ]
})
export class ContactNotSelectedComponent {
}
