import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Observable} from 'rxjs';
import {Contact} from '../../models/contact.model';
import {ContactsFacade} from '../../contacts.facade';

@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html'
})
export class ContactListComponent implements OnInit {

  @Output() contactClick: EventEmitter<Contact> = new EventEmitter<Contact>();

  constructor(private readonly contactsFacade: ContactsFacade) {
  }

  // Getters
  get contacts$(): Observable<Contact[]> {
    return this.contactsFacade.contacts$();
  }

  // Event Handlers
  onContactClicked(contact: Contact): void {
    this.contactClick.emit(contact);
  }

  // Lifecycles
  ngOnInit(): void {
    this.contactsFacade.fetchContacts();
  }
}
