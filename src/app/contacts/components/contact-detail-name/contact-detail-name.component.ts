import {Component, Input} from '@angular/core';
import {ContactName, ContactPicture} from '../../models/contact.model';

@Component({
  selector: 'app-contact-detail-name',
  templateUrl: './contact-detail-name.component.html',
  styleUrls: ['./contact-detail-name.component.css']
})
export class ContactDetailNameComponent {
  @Input() contactName: ContactName;
  @Input() contactPicture: ContactPicture;
}
