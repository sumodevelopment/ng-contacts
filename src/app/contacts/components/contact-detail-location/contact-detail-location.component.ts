import {Component, Input} from '@angular/core';
import {ContactLocation} from '../../models/contact.model';

@Component({
  selector: 'app-contact-detail-location',
  templateUrl: './contact-detail-location.component.html',
  styleUrls: ['./contact-detail-location.component.css']
})
export class ContactDetailLocationComponent {
  @Input() contactLocation: ContactLocation;
}
