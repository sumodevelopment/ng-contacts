import {NgModule} from '@angular/core';
import {SharedModule} from '../shared/shared.module';
import {ContactsPage} from './pages/contacts/contacts.page';
import {ContactsRoutingModule} from './contacts-routing.module';
import {ContactDetailPage} from './pages/contact-detail/contact-detail.page';
import {ContactListComponent} from './components/contact-list/contact-list.component';
import {ContactListItemComponent} from './components/contact-list-item/contact-list-item.component';
import {ContactNotSelectedComponent} from './components/contact-not-selected/contact-not-selected.component';
import {ContactDetailNameComponent} from './components/contact-detail-name/contact-detail-name.component';
import {ContactDetailLocationComponent} from './components/contact-detail-location/contact-detail-location.component';

@NgModule({
  declarations: [
    // Pages
    ContactsPage,
    ContactDetailPage,
    // Components
    ContactListComponent,
    ContactListItemComponent,
    ContactNotSelectedComponent,
    ContactDetailNameComponent,
    ContactDetailLocationComponent
  ],
  imports: [
    SharedModule,
    ContactsRoutingModule
  ]
})
export class ContactsModule {
}
