import {Injectable} from '@angular/core';
import {ContactsAPI} from './api/contacts.api';
import {HttpErrorResponse} from '@angular/common/http';
import {ContactsState} from './state/contacts.state';
import {Contact} from './models/contact.model';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ContactsFacade {

  constructor(private readonly contactsAPI: ContactsAPI, private contactState: ContactsState) {
  }

  contacts$(): Observable<Contact[]> {
    return this.contactState.getContacts$();
  }

  fetchContacts(): void {
    this.contactsAPI.fetchContacts$()
      .pipe(
        map((contacts: Contact[]) => {
          return contacts.sort((a: Contact, b: Contact) => {
            return new Intl.Collator().compare(a.name.last, b.name.last);
          });
        })
      )
      .subscribe((contacts: Contact[]) => {
        this.contactState.setContacts(contacts);
      }, (error: HttpErrorResponse) => {
        this.contactState.setError(error.message);
      });
  }

}
