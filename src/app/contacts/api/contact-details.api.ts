import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Contact} from '../models/contact.model';
import {environment} from '../../../environments/environment';

const {contactsApiBaseUrl} = environment;

@Injectable({
  providedIn: 'root'
})
export class ContactDetailsAPI {
  constructor(private readonly http: HttpClient) {
  }

  fetchContactByUuid(uuid: string): Observable<Contact[]> {
    return this.http.get<Contact[]>(`${contactsApiBaseUrl}/contacts?login.uuid=${uuid}`);
  }
}
