import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {Contact} from '../models/contact.model';

const {contactsApiBaseUrl} = environment;

@Injectable({
  providedIn: 'root'
})
export class ContactsAPI {
  constructor(private readonly http: HttpClient) {
  }

  public fetchContacts$(): Observable<Contact[]> {
    return this.http.get<Contact[]>(`${contactsApiBaseUrl}/contacts`);
  }
}
