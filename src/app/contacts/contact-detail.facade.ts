import {Injectable} from '@angular/core';
import {ContactDetailState} from './state/contact-detail.state';
import {ContactDetailsAPI} from './api/contact-details.api';
import {Observable} from 'rxjs';
import {Contact} from './models/contact.model';
import {map} from 'rxjs/operators';
import {HttpErrorResponse} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ContactDetailFacade {
  constructor(
    private readonly contactDetailState: ContactDetailState,
    private readonly contactDetailAPI: ContactDetailsAPI) {
  }

  contact$(): Observable<Contact> {
    return this.contactDetailState.getContact$();
  }

  error$(): Observable<string> {
    return this.contactDetailState.getError$();
  }

  fetchContactByUuid(uuid: string): void {
    this.contactDetailState.setError('');

    this.contactDetailAPI.fetchContactByUuid(uuid)
      .pipe(
        map((contacts: Contact[]) => {
          if (contacts.length === 0) {
            throw Error('The contact does not exist.');
          }
          return contacts.pop();
        })
      ).subscribe((contact: Contact) => {
      this.contactDetailState.setContact(contact);
    }, (error: HttpErrorResponse) => {
      this.contactDetailState.setError(error.message);
    });
  }
}
