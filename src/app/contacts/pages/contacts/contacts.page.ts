import {Component} from '@angular/core';
import {Contact} from '../../models/contact.model';
import {Router} from '@angular/router';
import {AppRoutes} from '../../../shared/enums/app-routes.enum';
import {ContactDetailFacade} from '../../contact-detail.facade';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-contacts-page',
  templateUrl: './contacts.page.html',
  styleUrls: ['./contacts.page.css']
})
export class ContactsPage {

  constructor(private readonly router: Router, private readonly contactDetailFacade: ContactDetailFacade) {
  }

  get contact$(): Observable<Contact> {
    return this.contactDetailFacade.contact$();
  }

  handleContactClicked(contact: Contact): Promise<boolean> {
    // set the contact that was clicked
    return this.router.navigate([AppRoutes.Contacts, contact.login.uuid]);
  }

}
