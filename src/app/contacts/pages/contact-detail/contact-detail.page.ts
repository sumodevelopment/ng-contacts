import {Component} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ContactDetailFacade} from '../../contact-detail.facade';
import {Observable} from 'rxjs';
import {Contact} from '../../models/contact.model';

@Component({
  selector: 'app-contact-detail-page',
  templateUrl: './contact-detail.page.html',
  styleUrls: ['./contact-detail.page.css']
})
export class ContactDetailPage {
  public contactUuid: string = null;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly contactDetailFacade: ContactDetailFacade
  ) {

    this.route.params.subscribe(({id}) => {
      this.contactUuid = id;
      this.fetchContact();
    });
  }

  get contact$(): Observable<Contact> {
    return this.contactDetailFacade.contact$();
  }

  get error$(): Observable<string> {
    return this.contactDetailFacade.error$();
  }

  private fetchContact(): void {
    this.contactDetailFacade.fetchContactByUuid(this.contactUuid);
  }
}
