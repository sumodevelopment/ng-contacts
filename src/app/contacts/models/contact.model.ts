export interface ContactName {
  title: string;
  first: string;
  last: string;
}

export interface ContactLocation {
  street: ContactLocationStreet;
  city: string;
  state: string;
  country: string;
  postcode: number;
  coordinates: any;
  timezone: any;
}

export interface ContactLocationStreet {
  number: number;
  name: string;
}

export interface ContactLocationCoordinates {
  latitude: string;
  longitude: string;
}

export interface ContactLocationTimezone {
  offset: string;
  description: string;
}

export interface ContactDob {
  date: string;
  age: number;
}

export interface ContactPicture {
  large: string;
  medium: string;
  thumbnail: string;
}

export interface ContactLogin {
  md5: string;
  password: string;
  salt: string;
  sha1: string;
  sha256: string;
  username: string;
  uuid: string;
}

export interface Contact {
  id: string;
  gender: string;
  name: ContactName;
  location: ContactLocation;
  email: string;
  dob: ContactDob;
  phone: string;
  cell: string;
  picture: ContactPicture;
  nat: string;
  login: ContactLogin;
}

