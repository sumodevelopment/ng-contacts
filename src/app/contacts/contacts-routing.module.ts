import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ContactsPage} from './pages/contacts/contacts.page';
import {ContactDetailPage} from './pages/contact-detail/contact-detail.page';

const routes: Routes = [
  {
    path: '',
    component: ContactsPage,
    children: [
      {
        path: ':id',
        component: ContactDetailPage
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class ContactsRoutingModule {
}
