import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {Contact} from '../models/contact.model';

@Injectable({
  providedIn: 'root'
})
export class ContactDetailState {

  private readonly contact$: BehaviorSubject<Contact> = new BehaviorSubject<Contact>(null);
  private readonly error$: BehaviorSubject<string> = new BehaviorSubject<string>('');

  public getContact$(): Observable<Contact> {
    return this.contact$.asObservable();
  }

  public setContact(contact: Contact): void {
    this.contact$.next(contact);
  }

  public getError$(): Observable<string> {
    return this.error$.asObservable();
  }

  public setError(error: string): void {
    this.error$.next(error);
  }

}
