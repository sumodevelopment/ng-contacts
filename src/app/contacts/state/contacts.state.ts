import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {Contact} from '../models/contact.model';

@Injectable({
  providedIn: 'root'
})
export class ContactsState {

  private readonly contacts$: BehaviorSubject<Contact[]> = new BehaviorSubject<Contact[]>([]);
  private readonly error$: BehaviorSubject<string> = new BehaviorSubject<string>('');

  getContacts$(): Observable<Contact[]> {
    return this.contacts$.asObservable();
  }

  setContacts(contacts: Contact[]): void {
    this.contacts$.next(contacts);
  }

  getError$(): Observable<string> {
    return this.error$.asObservable();
  }

  setError(error: string): void {
    this.error$.next(error);
  }

}
