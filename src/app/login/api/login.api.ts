import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {environment} from 'src/environments/environment';
import {User} from 'src/app/shared/models/user.model';

const {contactsApiBaseUrl} = environment;

@Injectable({
  providedIn: 'root'
})
export class LoginAPI {

  constructor(private readonly http: HttpClient) {
  }

  public login$(username: string): Observable<User[]> {
    return this.http.get<User[]>(`${contactsApiBaseUrl}/users?username=${username}&active=true`);
  }

}
