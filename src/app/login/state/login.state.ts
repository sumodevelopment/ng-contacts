import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {User} from '../../shared/models/user.model';

@Injectable({
  providedIn: 'root'
})
export class LoginState {

  private readonly error$: BehaviorSubject<string> = new BehaviorSubject<string>('');
  private readonly user$: BehaviorSubject<User> = new BehaviorSubject<User>(null);
  private readonly loading$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  public getError$(): Observable<string> {
    return this.error$.asObservable();
  }

  public setError(error: string): void {
    this.error$.next(error);
  }

  public getUser$(): Observable<User> {
    return this.user$.asObservable();
  }

  public setUser(user: User): void {
    this.user$.next(user);
  }

  public getLoading$(): Observable<boolean> {
    return this.loading$.asObservable();
  }

  public setLoading(loading: boolean): void {
    this.loading$.next(loading);
  }

}
