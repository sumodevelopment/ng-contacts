import {Component, EventEmitter, OnDestroy, Output} from '@angular/core';
import {LoginFacade} from '../../login.facade';
import {Observable, Subscription} from 'rxjs';
import {User} from '../../../shared/models/user.model';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html'
})
export class LoginFormComponent implements OnDestroy {

  // Communication
  @Output() successful: EventEmitter<User> = new EventEmitter<User>();

  // Properties
  public username: string = '';

  // Subscriptions
  private user$: Subscription;

  constructor(private readonly loginFacade: LoginFacade) {
    this.user$ = this.loginFacade.user$().subscribe((user: User) => {
      if (user === null) {
        return;
      }
      this.successful.emit(user);
    });
  }

  // GETTERS
  get error$(): Observable<string> {
    return this.loginFacade.error$();
  }

  get loading$(): Observable<boolean> {
    return this.loginFacade.loading$();
  }

  // EVENT HANDLERS
  public onLoginClick(): void {
    this.loginFacade.login(this.username);
  }

  // Lifecycles
  ngOnDestroy(): void {
    this.user$.unsubscribe();
  }

}
