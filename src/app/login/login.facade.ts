import {Injectable} from '@angular/core';
import {HttpErrorResponse} from '@angular/common/http';
import {LoginAPI} from './api/login.api';
import {LoginState} from './state/login.state';
import {User} from 'src/app/shared/models/user.model';
import {finalize, map} from 'rxjs/operators';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginFacade {
  constructor(private readonly loginAPI: LoginAPI, private readonly loginState: LoginState) {
  }

  public error$(): Observable<string> {
    return this.loginState.getError$();
  }

  public user$(): Observable<User> {
    return this.loginState.getUser$();
  }

  public loading$(): Observable<boolean> {
    return this.loginState.getLoading$();
  }

  public login(username: string): void {

    this.loginState.setLoading(true);

    this.loginAPI.login$(username)
      .pipe(
        map((response: User[]) => {
          if (response.length === 0) {
            throw Error(`User ${username} was not found.`);
          }
          return response.pop();
        }),
        finalize(() => {
          this.loginState.setLoading(false);
        })
      )
      .subscribe((user: User) => {
        this.loginState.setUser(user);
      }, (error: HttpErrorResponse) => {
        this.loginState.setError(error.message);
      });
  }

}
