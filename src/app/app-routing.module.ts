import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AppRoutes} from './shared/enums/app-routes.enum';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: `/${AppRoutes.Login}`
  },
  {
    path: AppRoutes.Login,
    loadChildren: () => import('./login/login.module').then(m => m.LoginModule)
  },
  {
    path: AppRoutes.Contacts,
    loadChildren: () => import('./contacts/contacts.module').then(m => m.ContactsModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {
}
