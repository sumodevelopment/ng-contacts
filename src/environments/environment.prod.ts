export const environment = {
  production: true,
  contactsApiBaseUrl: 'https://noroff-contacts-api.herokuapp.com'
};
