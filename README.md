# Ng Contacts

## Login /login - Page

## LoginForm [ Component ]
Allow a user to login user their username

## Contacts /contacts - Contacts

### ContactList [ Component ]
Sidebar with all the contacts in alphabetical order
Search contacts in the sidebar.

### ContactDetail [ Component ]
Detail section, displaying selected contact information
Edit That contact

